<?php

namespace Drupal\financial_calculators\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'PatronageCalculator' block.
 *
 * @Block(
 *  id = "patronage_calculator",
 *  admin_label = @Translation("Patronage Calculator"),
 * )
 */
class PatronageCalculator extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $config = \Drupal::config('financial_calculators.settings');

    $build['patronage_calculator'] = [
      '#theme' => 'financial_calculators_patronage',
      '#attributes' => [
        'class' => ['patronage-calculator'],
      ],
      '#attached' => [
        'library' => ['financial_calculators/patronage_calculator'],
      ],
      '#patronage_percent' => $config->get('patronage_percent')
    ];

    return $build;
  }

}
