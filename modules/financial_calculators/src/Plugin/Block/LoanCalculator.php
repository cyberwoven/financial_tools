<?php

namespace Drupal\financial_calculators\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'LoanCalculator' block.
 *
 * @Block(
 *  id = "loan_calculator",
 *  admin_label = @Translation("Loan Payment Calculator"),
 * )
 */
class LoanCalculator extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $config = \Drupal::config('financial_calculators.settings');

    $build['loan_calculator'] = [
      '#theme' => 'financial_calculators_loan',
      '#attributes' => [
        'class' => ['loan-calculator'],
      ],
      '#attached' => [
        'library' => ['financial_calculators/loan_calculator'],
      ],
      '#loan_calc_amount' => $config->get('loan_calc_amount'),
      '#loan_calc_interest' => $config->get('loan_calc_interest'),
      '#loan_calc_term' => $config->get('loan_calc_term'),
      '#loan_calc_frequency' => $config->get('loan_calc_frequency'),
    ];

    return $build;
  }

}
