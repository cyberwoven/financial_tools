<?php

/**
 * @file
 * Contains \Drupal\financial_extlink_override\Plugin\Block\FinancialLinkMessage.
 */

namespace Drupal\financial_extlink_override\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'FinancialLinkMessage' block.
 *
 * @Block(
 *  id = "financial_link_message",
 *  admin_label = @Translation("Ag First link message"),
 * )
 */
class FinancialLinkMessage extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('extlink.settings');
    $build = [];
    $block = [
      '#theme' => 'financial_extlink_override_link_message',
      '#attributes' => [
        'class' => ['financial-link-dialog'],
        'id' => 'financial-link-dialog-block',
      ],
      '#message' => $config->get('extlink_alert_text'),
      '#cache' => [
        'max-age' => Cache::PERMANENT, // TODO: link CacheTag to config message
      ],
    ];

    $build['financial_link_message'] = $block;
    return $build;
  }

}
