<?php

namespace Drupal\financial_drush_commands\Commands;

use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\financial_drush_commands\Commands
 */
class FinancialDrushCommands extends DrushCommands {
  /**
   * Drush command that displays the given text.
   *
   *   Argument with message to be displayed.
   * @command financial_drush_commands:replaceWithHubspot
   * @aliases replace-hs
   */
  public function replaceWithHubspot($site = '') {
    $old_new = [
      'n9ix6rfeei39gbqvqnzyw' => '9906fa01-2938-4f8f-a8ff-894a4fac2f67',
      'snsqwqxdeeiakqbqvqnzyw' => 'b5a918dd-16f4-4017-a979-f1664587ef72',
      '4erfrmuyeeacwgbqvqmgoa' => '781f9749-fa61-4e82-b707-3c391c668004',
      'mxqqdqzhee2b3wbqvqmudq' => 'c1120038-c741-427c-91ff-1a32733b7ff0',
      'rvgbekcgee2b5abqvqmqfa' => '1c905179-1781-4d47-b672-e18bcdaddd80',
      'x0zlakctee2b5abqvqmqfa' => 'ed665fd7-62bb-4b8e-a011-fb9b45afcd9e',
      'iq79xodueeeevgbqvqmr2a' => 'af88e034-cf39-48de-84ec-3a1429838115',
      'i37lerm4ee2bxwbqvqmudq' => 'fb68e34-c765-4c70-b45f-38e1d1d18296',
      'gmvswrrbeeeliwbqvqnzyw' => '1750d948-ad77-487f-9ce0-0283771453d1',
      '2qpmkhteee2b1wbqvqmudq' => '09778961-9a7f-4d78-a6c0-d2e38a7b881e',
      'sf0fiofoee2b3wbqvqmqfa' => '92abf01e-ac83-4a31-a85e-b92dd35df47e',
      'emhhwjsweeybtgbqvqmudq' => 'f5aaf186-deb7-4e64-8657-780c99b1f608',
      '8exi7qccee2b3wbqvqmudq' => '10b3986f-d15a-4c52-9cde-a253f5d4be4e',
      'vshs1vdzeeybsgbqvqmqfa' => 'd3aad954-e146-4a6f-97e2-10bb4c38a7cb',
      'cmxrjiylee2b2qbqvqmudq' => '3061189b-cefe-4c9f-b351-02ae48a5576d',
      'jssn8bpleeenagbqvqmgoa' => '35822ee9-e256-41b6-a099-4297d2e6ab0e',
      'ewzitnzteeqbfgbqvqmqfa' => '43ef36f1-750a-4911-a7f2-c69010691d09',
      'm6xebvfueeybwwbqvqmudq' => '139e7b34-b3de-4696-92b4-8f4c97ecc96d',
      '47vhypgceeybwwbqvqmudq' => '47abc3a3-c305-42b8-9465-07dd9ded176d',
      'kpynffgbeeybwwbqvqmudq' => '7729986d-d5cc-4c18-a0ac-145e8ccd4136',
      'qs2spijeeybwwbqvqmudq' => 'cf338961-752a-4030-bd57-adfd33a056f5',
      'or5sdfgheeybwwbqvqmudq' => '840d42ec-0300-429b-af40-1c0414c29a40',
      'nky6vfgheeybwwbqvqmudq' => '8b73b5e4-4e09-4f62-b5e1-11ee2ec42080',
      'lekbsgceeybwwbqvqmudq' => '49ce58d1-963d-4725-a7e6-8da6e99002cb',
      'g3aowff1eeybwwbqvqmudq' => '889f88a8-f4f0-4b41-a762-b11f173049eb',
      'ljogbeeybwwbqvqmudq' => 'cfa7a452-fd83-413d-97a6-46a8140c8640',
      '47vhxgceeybwwbqvqmudq' => '0aa68fda-b422-4981-9063-356eb290dba6',
      'deq7spf2eeybwwbqvqmudq' => '1ccc3087-fad6-4f6e-bd8d-721ad2d1b8d6',
      'cz0gspgaeeybwwbqvqmudq' => '51741e7f-be6f-418e-951f-687aa5974de5',
      'nkex8pf0eeybwwbqvqmudq' => '9761fa40-3fec-4ce3-b73a-2382cf1e2dcf',
      'ug6cpggeeybwwbqvqmudq' => 'bdf776b0-2cc6-4631-9dc0-6d3b9eb7bc0b',
      '7pgrtfgheeybwwbqvqmudq' => 'ba3ba0d9-9876-45f7-9978-7808c964ae3b',
      '74fifgceeybwwbqvqmudq' => '572181ad-099f-4a5b-8492-aaba9e7fc09f',
      'i69xpgceeybwwbqvqmudq' => 'ade1a256-369e-40ef-9fec-a72c3b989eab',
      'nfxojpgbeeybwwbqvqmudq' => 'a22f900f-6a6b-4b2a-96ee-b952959977ec',
      'pvsarvgeeeybwwbqvqmudq' => 'b43c4291-1721-457b-bbd2-32c4fd534043',
      'yzjuefgaeeybwwbqvqmudq' => '84dab334-60ee-4a12-ac59-b591766a5489',
      'mz1gceeybwwbqvqmudq' => '2a717f62-73c3-4d16-9a76-3c56eaa99881',
      '3vaykgeeeybwwbqvqmudq' => 'ad3280d8-7393-41c2-9f92-0983f3727935',
      '3qxmqgceeybwwbqvqmudq' => '50f4fdd4-27fc-4e06-bf38-d29ed3859e4f',
      '50f4fdd4-27fc-4e06-bf38-d29ed3859e4f' => 'da2509e4-fc6d-4c73-9a58-68ec84e7e23c',
      'smwysesgeei70gbqvqmgoa' => 'fac5637b-ab24-4d44-8da9-470121c6b46b',
      '40wfnlooeembswbqvqmudq' => 'aea473c3-3600-4ca2-9757-fcae9946071f',
      'nahc6ejdeeimbgbqvqmr2a' => 'da2509e4-fc6d-4c73-9a58-68ec84e7e23c',
      'xzvsa4ffee2b3wbqvqmqfa' => '9906fa01-2938-4f8f-a8ff-894a4fac2f67',
    ];

    $field = 'field_embedded_form';
    if ($site == 'rivervalleyfarmcredit') {
      $field = 'field_form';
    }
    if ($site == 'centralkentuckyag') {
      $field = 'field_embed';
    }
    if ($site == 'aggeorgia') {
      $field = 'field_form';
    }
    if ($site == 'arborone') {
      $field = 'field_form_id';
    }
    if ($site == 'colonialfarmcredit') {
      $field = 'field_form';
    }
    if ($site == 'agsouthfc') {
      $field = 'field_embedded';
    }
    foreach ($old_new as $old => $new) {
      $old_form_query = \Drupal::entityQuery('paragraph')->condition("$field.form_code",$old, '=')->execute();
      $old_form_items =  \Drupal\paragraphs\Entity\Paragraph::loadMultiple($old_form_query);
      foreach ($old_form_items as $pid => $paragraph) {
        $new_form = [
          'form_code' => $new,
          'form_type' => 'hubspot'
        ];
        $paragraph->set($field, $new_form);
        $paragraph->save();
      }
    }
  }

  /**
   * Drush command that displays the given text.
   *
   *   Argument with message to be displayed.
   * @command financial_drush_commands:replaceAccountAccess
   * @aliases replace-aa
   */
  public function replaceAccountAccess() {
    $host = \Drupal::request()->getHost();
    $links = [
        'https://online.farmcredit.net/accounts/?ACA=012' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=012',
        'https://online.farmcredit.net/accounts/?ACA=085' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=085',
        'https://online.farmcredit.net/accounts/?ACA=052' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=017',
        'https://online.farmcredit.net/accounts/?ACA=057' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=057',
        'https://online.farmcredit.net/accounts/?ACA=042' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=042',
        'https://online.farmcredit.net/accounts/?ACA=087' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=087',
        'https://online.farmcredit.net/accounts/?ACA=033' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=033',
        'https://online.farmcredit.net/accounts/?ACA=076' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=076',
        'https://online.farmcredit.net/accounts/?ACA=078' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=078',
        'https://online.farmcredit.net/accounts/?ACA=081' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=081',
        'https://online.farmcredit.net/accounts/?ACA=014' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=014',
        'https://online.farmcredit.net/accounts/?ACA=092' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=092',
        'https://online.farmcredit.net/accounts/?ACA=032' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=032',
        'https://online.farmcredit.net/accounts/?ACA=086' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=086',
        'https://online.farmcredit.net/accounts/?ACA=050' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=050',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=012' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=012',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=085' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=085',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=052' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=017',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=057' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=057',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=042' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=042',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=087' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=087',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=033' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=033',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=076' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=076',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=078' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=078',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=081' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=081',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=014' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=014',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=092' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=092',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=032' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=032',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=086' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=086',
        'https://online.farmcredit.net/accounts/login.aspx?ACA=050' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=050',
        'https://online.farmcredit.net/accounts/login.aspx?aca=012' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=012',
        'https://online.farmcredit.net/accounts/login.aspx?aca=085' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=085',
        'https://online.farmcredit.net/accounts/login.aspx?aca=052' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=017',
        'https://online.farmcredit.net/accounts/login.aspx?aca=057' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=057',
        'https://online.farmcredit.net/accounts/login.aspx?aca=042' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=042',
        'https://online.farmcredit.net/accounts/login.aspx?aca=087' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=087',
        'https://online.farmcredit.net/accounts/login.aspx?aca=033' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=033',
        'https://online.farmcredit.net/accounts/login.aspx?aca=076' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=076',
        'https://online.farmcredit.net/accounts/login.aspx?aca=078' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=078',
        'https://online.farmcredit.net/accounts/login.aspx?aca=081' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=081',
        'https://online.farmcredit.net/accounts/login.aspx?aca=014' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=014',
        'https://online.farmcredit.net/accounts/login.aspx?aca=092' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=092',
        'https://online.farmcredit.net/accounts/login.aspx?aca=032' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=032',
        'https://online.farmcredit.net/accounts/login.aspx?aca=086' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=086',
        'https://online.farmcredit.net/accounts/login.aspx?aca=050' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=050',
        'https://online.farmcredit.net/accounts/?ACA=50' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=050',
        'https://online.farmcredit.net/accounts/?ACA=42' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=042',
        'https://online.farmcredit.net/accounts/?ACA=003' => 'https://digitalbanking.farmcredit.net/digitalbanking/?ACA=003',
    ];

    $nodeids = \Drupal::entityQuery('node')->accessCheck(FALSE)->condition('title','accountaccess', 'CONTAINS')->execute();
    $nodes =  \Drupal\node\Entity\Node::loadMultiple($nodeids);
    $this->replaceAccountAccessNode($nodes, $links, $host);

    $nodeids_two = \Drupal::entityQuery('node')->accessCheck(FALSE)->condition('body','accountaccess', 'CONTAINS')->execute();
    $nodes_two =  \Drupal\node\Entity\Node::loadMultiple($nodeids_two);
    $this->replaceAccountAccessNode($nodes_two, $links, $host);

    $nodeids_three = \Drupal::entityQuery('node')->accessCheck(FALSE)->condition('title','account access', 'CONTAINS')->execute();
    $nodes_three =  \Drupal\node\Entity\Node::loadMultiple($nodeids_three);
    $this->replaceAccountAccessNodeTwo($nodes_three, $links, $host);

    $nodeids_four = \Drupal::entityQuery('node')->accessCheck(FALSE)->condition('body','account access', 'CONTAINS')->execute();
    $nodes_four =  \Drupal\node\Entity\Node::loadMultiple($nodeids_four);
    $this->replaceAccountAccessNodeTwo($nodes_four, $links, $host);

    $menu_items_query = \Drupal::entityQuery('menu_link_content')->accessCheck(FALSE)->condition('title','accountaccess', 'CONTAINS')->execute();
    $menu_items =  \Drupal\menu_link_content\Entity\MenuLinkContent::loadMultiple($menu_items_query);
    $this->replaceAccountAccessMenu($menu_items, $links, $host);
  }

  public function replaceAccountAccessNode($nodes, $links, $host) {
    foreach ($nodes as $nid => $node) {
      $title = $node->get('title')->value;
      $title = str_replace('AccountAccess', 'Digital Banking', $title);
      $title = str_replace('accountaccess', 'Digital Banking', $title);
      $title = str_replace('Accountaccess', 'Digital Banking', $title);
      $node->set('title', $title);
      $body = $node->get('body')->value;
      $body_summary = $node->get('body')->summary;
      $body_format = $node->get('body')->format;
      $body = str_replace('AccountAccess', 'Digital Banking', $body);
      $body = str_replace('accountaccess', 'Digital Banking', $body);
      $body = str_replace('Accountaccess', 'Digital Banking', $body);
      foreach ($links as $old_link => $new_link) {
        $body = str_replace($old_link, $new_link, $body);
      }
      $node->set('body', [
        'value' => $body,
        'summary' => $body_summary,
        'format' => $body_format
      ]);
      $node->save();
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);
      \Drupal::logger('DigitalBanking')->info($host . $alias);
    }
  }

  public function replaceAccountAccessNodeTwo($nodes, $links, $host) {
    foreach ($nodes as $nid => $node) {
      $title = $node->get('title')->value;
      $title = str_replace('Account Access', 'Digital Banking', $title);
      $title = str_replace('account access', 'Digital Banking', $title);
      $title = str_replace('Account access', 'Digital Banking', $title);
      $node->set('title', $title);
      $body = $node->get('body')->value;
      $body_summary = $node->get('body')->summary;
      $body_format = $node->get('body')->format;
      $body = str_replace('Account Access', 'Digital Banking', $body);
      $body = str_replace('account access', 'Digital Banking', $body);
      $body = str_replace('Account access', 'Digital Banking', $body);
      foreach ($links as $old_link => $new_link) {
        $body = str_replace($old_link, $new_link, $body);
      }
      $node->set('body', [
        'value' => $body,
        'summary' => $body_summary,
        'format' => $body_format
      ]);
      $node->save();
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);
      \Drupal::logger('DigitalBanking')->info($host . $alias);
    }
  }

  public function replaceAccountAccessMenu($menu_items, $links, $host) {
    foreach ($menu_items as $menu_item) {
      $link = $menu_item->get('link')->uri;
      $link_options = $menu_item->get('link')->options;
      foreach ($links as $old_link => $new_link) {
        $link = str_replace($old_link, $new_link, $link);
      }
      $title = $menu_item->get('title')->value;
      $title = str_replace('Online Banking with AccountAccess', 'Digital Banking', $title);
      $title = str_replace('AccountAccess', 'Digital Banking', $title);
      $title = str_replace('accountaccess', 'Digital Banking', $title);
      $title = str_replace('Accountaccess', 'Digital Banking', $title);
      $menu_item->set('link', [
        'uri' => $link,
        'title' => $title,
        'options' => $link_options
      ]);
      $menu_item->set('title', $title);

      \Drupal::logger('DigitalBanking')->info($host . ': Menu item - AccountAccess');
      $menu_item->save();
    }
  }

}
