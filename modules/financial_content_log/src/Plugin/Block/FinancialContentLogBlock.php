<?php

namespace Drupal\financial_content_log\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a global content modification log on Drupal sites.
 *
 * @Block(
 *   id = "financial_content_log",
 *   admin_label = @Translation("Content Log"),
 * )
 */
class FinancialContentLogBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['financial_content_log_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content Log Configuration Field'),
      '#size' => 60,
      '#description' => $this->t('A field to store a configuration value.'),
      '#default_value' => (empty($config['financial_content_log_field']) ? '' : $config['financial_content_log_field']),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $config_value = (empty($config['financial_content_log_field']) ? '' : $config['financial_content_log_field']);


    // Build the block.
    $build = [];

    $block = [
      '#theme' => 'financial_content_log',
      '#attributes' => [
        'class' => ['financial-content-log'],
        'id' => $this->getBaseId(),
      ],
      '#content' => [
        'cid' => $config_value,
      ],
      '#cache' => [
        'tags' => ['config:financial_content_log.settings']
      ],
      '#attached' => [
        'library' => 'financial_content_log/financial_content_log.functions',
        'drupalSettings' => [
          'json_url' => $config_value,
        ],
      ]
    ];

    $build['financial-content-log-block'] = $block;

    return $build;

  }


  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['financial_content_log_field'] = $values['financial_content_log_field'];
  }

}
